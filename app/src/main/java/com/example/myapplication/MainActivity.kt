package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.annotation.MainThread
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.math.BigInteger
import java.util.*
import java.util.concurrent.CountDownLatch
import kotlin.concurrent.thread

private const val COUNT = 10

class MainActivity : AppCompatActivity() {

    private val latch = CountDownLatch(COUNT)
    private val random = Random()
    private lateinit var messageView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        messageView = findViewById(R.id.message)

        thread {
            var time = System.currentTimeMillis()
            latch.await()
            time = System.currentTimeMillis() - time
            runOnUiThread {
                appendText("$time ms")
            }
        }
        for (i in 1..COUNT) {
            CoroutineScope(Dispatchers.Main).launch {
                withContext(Dispatchers.IO) {
                    Thread.sleep(10000L)
                    val value = BigInteger(100, random).toString(26)
                    runOnUiThread {
                        appendText("$i: $value")
                    }
                    latch.countDown()
                }
            }
        }
    }

    @MainThread
    private fun appendText(text: String) {
        messageView.text = "${messageView.text}\n$text"
    }
}
